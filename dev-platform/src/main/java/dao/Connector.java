package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connector {

    public static Connection getConnection() throws SQLException {

        return DriverManager.getConnection(
                AppProperties.getInstance().getProp("DATABASE_URL"));
    }
}
