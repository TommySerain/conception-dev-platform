package dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

import entities.Developer;

public class DeveloperDao extends AbstractDao<Developer> {

    public DeveloperDao(String addSQL, String getAllSQL, String getByIdSQL, String updateSQL, String deleteSQL,
            int dayRate) {
        super(
                "INSERT INTO Developer (firstname, lastname, mail, password, phonenumber, birthdate, image, address, city, dayRate) VALUES (?,?,?,?,?,?,?,?,?,?)",
                "SELECT * FROM Developer",
                "SELECT * FROM Developer WHERE id=?",
                "UPDATE Developer SET firstname=?,lastname=?,mail=?, password=?, phonenumber=?, birthdate=?, image=?, address=?, city=?, dayRate=? WHERE id=?",
                "DELETE FROM Developer WHERE id=?");
    }

    @Override
    protected Developer sqlToEntity(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        String firstname = rs.getString("firstname");
        String lastname = rs.getString("lastname");
        String mail = rs.getString("mail");
        String password = rs.getString("password");
        String phonenumber = rs.getString("phonenumber");
        Date sqlDate = rs.getDate("birthdate");
        LocalDate birthdate = sqlDate.toLocalDate();
        String image = rs.getString("image");
        String address = rs.getString("address");
        String city = rs.getString("city");
        int dayRate = rs.getInt("dayRate");
        return new Developer(id, firstname, lastname, mail, password, phonenumber, birthdate, image, address, city,
                dayRate);
    }

    @Override
    protected void setEntityId(Developer entity, int id) {
        entity.setId(id);
    }

    @Override
    protected void entityBindValues(PreparedStatement stmt, Developer entity) throws SQLException {
        stmt.setString(1, entity.getFirstname());
        stmt.setString(2, entity.getLastname());
        stmt.setString(3, entity.getMail());
        stmt.setString(4, entity.getPassword());
        stmt.setString(5, entity.getPhonenumber());
        stmt.setDate(6, Date.valueOf(entity.getBirthdate()));
        stmt.setString(7, entity.getImage());
        stmt.setString(8, entity.getAddress());
        stmt.setString(9, entity.getCity());
        stmt.setInt(10, entity.getDayRate());
    }

    @Override
    protected void entityBindValuesWithId(PreparedStatement stmt, Developer entity) throws SQLException {
        entityBindValues(stmt, entity);
        stmt.setInt(11, entity.getId());
    }

}
