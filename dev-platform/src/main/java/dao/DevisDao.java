package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import entities.Devis;

public class DevisDao extends AbstractDao<Devis> {

    public DevisDao(String addSQL, String getAllSQL, String getByIdSQL, String updateSQL, String deleteSQL) {
        super(
                "INSERT INTO Devis (quoteNumber, totalPrice, type, duration, status) VALUES (?,?,?,?,?)",
                "SELECT * FROM Devis",
                "SELECT * FROM Devis WHERE id=?",
                "UPDATE Devis SET quoteNumber=?, totalPrice=?, type=?, duration=?, status=? WHERE id=?",
                "DELETE FROM Devis WHERE id=?");
    }

    @Override
    protected void setEntityId(Devis entity, int id) {
        entity.setId(id);
    }

    @Override
    protected void entityBindValues(PreparedStatement stmt, Devis entity) throws SQLException {
        stmt.setString(1, entity.getQuoteNumber());
        stmt.setInt(2, entity.getTotalPrice());
        stmt.setString(3, entity.getType());
        stmt.setInt(4, entity.getDuration());
        stmt.setString(5, entity.getStatus());
    }

    @Override
    protected void entityBindValuesWithId(PreparedStatement stmt, Devis entity) throws SQLException {
        entityBindValues(stmt, entity);
        stmt.setInt(6, entity.getId());
    }

    @Override
    protected Devis sqlToEntity(ResultSet rs) throws SQLException {
        Integer id = rs.getInt("id");
        String quoteNumber = rs.getString("firstname");
        int totalPrice = rs.getInt("lastname");
        String type = rs.getString("mail");
        int duration = rs.getInt("password");
        String status = rs.getString("phonenumber");
        return new Devis(id, quoteNumber, totalPrice, type, duration, status);
    }

}
