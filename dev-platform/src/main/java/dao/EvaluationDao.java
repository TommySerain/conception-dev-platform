package dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

import entities.Evaluation;

public class EvaluationDao extends AbstractDao<Evaluation> {

    public EvaluationDao(String addSQL, String getAllSQL, String getByIdSQL, String updateSQL, String deleteSQL) {
        super(
                "INSERT INTO Evaluation (note, comments, date) VALUES (?,?,?)",
                "SELECT * FROM Evaluation",
                "SELECT * FROM Evaluation WHERE id=?",
                "UPDATE Evaluation SET note=?, comments=?, date=? WHERE id=?",
                "DELETE FROM Evaluation WHERE id=?");
    }

    @Override
    protected void setEntityId(Evaluation entity, int id) {
        entity.setId(id);
    }

    @Override
    protected void entityBindValues(PreparedStatement stmt, Evaluation entity) throws SQLException {
        stmt.setInt(1, entity.getNote());
        stmt.setString(2, entity.getComments());
        stmt.setDate(3, Date.valueOf(entity.getDate()));
    }

    @Override
    protected void entityBindValuesWithId(PreparedStatement stmt, Evaluation entity) throws SQLException {
        entityBindValues(stmt, entity);
        stmt.setInt(4, entity.getId());
    }

    @Override
    protected Evaluation sqlToEntity(ResultSet rs) throws SQLException {
        Integer id = rs.getInt("id");
        int note = rs.getInt("note");
        String comments = rs.getString("comment");
        Date sqlDate = rs.getDate("date");
        LocalDate date = sqlDate.toLocalDate();
        return new Evaluation(id, note, comments, date);
    }

}
