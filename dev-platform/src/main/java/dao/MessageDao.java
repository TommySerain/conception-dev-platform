package dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import entities.Message;

public class MessageDao extends AbstractDao<Message> {

    public MessageDao(String addSQL, String getAllSQL, String getByIdSQL, String updateSQL, String deleteSQL) {
        super(
                "INSERT INTO Message (content, date) VALUES (?,?)",
                "SELECT * FROM Message",
                "SELECT * FROM Message WHERE id=?",
                "UPDATE Message SET content=?, date=? WHERE id=?",
                "DELETE FROM Message WHERE id=?");
    }

    @Override
    protected void setEntityId(Message entity, int id) {
        entity.setId(id);
    }

    @Override
    protected void entityBindValues(PreparedStatement stmt, Message entity) throws SQLException {
        stmt.setString(1, entity.getContent());
        stmt.setDate(2, Date.valueOf(entity.getDate()));
    }

    @Override
    protected void entityBindValuesWithId(PreparedStatement stmt, Message entity) throws SQLException {
        entityBindValues(stmt, entity);
        stmt.setInt(3, entity.getId());
    }

    @Override
    protected Message sqlToEntity(ResultSet rs) throws SQLException {
        Integer id = rs.getInt("id");
        String content = rs.getString("content");
        Date sqlDate = rs.getDate("date");
        LocalDate date = sqlDate.toLocalDate();
        return new Message(id, content, date);
    }
}
