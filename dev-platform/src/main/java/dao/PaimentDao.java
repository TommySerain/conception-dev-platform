package dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import entities.Paiment;

public class PaimentDao extends AbstractDao<Paiment> {

    public PaimentDao(String addSQL, String getAllSQL, String getByIdSQL, String updateSQL, String deleteSQL) {
        super(
                "INSERT INTO Paiment (date, bankId, price, status) VALUES (?,?)",
                "SELECT * FROM Paiment",
                "SELECT * FROM Paiment WHERE id=?",
                "UPDATE Paiment SET date=?, bankId=?, price=?, status=? WHERE id=?",
                "DELETE FROM Paiment WHERE id=?");
    }

    @Override
    protected void setEntityId(Paiment entity, int id) {
        entity.setId(id);
    }

    @Override
    protected void entityBindValues(PreparedStatement stmt, Paiment entity) throws SQLException {
        stmt.setDate(1, Date.valueOf(entity.getDate()));
        stmt.setString(2, entity.getBankId());
        stmt.setFloat(3, entity.getPrice());
        stmt.setString(4, entity.getStatus());
    }

    @Override
    protected void entityBindValuesWithId(PreparedStatement stmt, Paiment entity) throws SQLException {
        entityBindValues(stmt, entity);
        stmt.setInt(5, entity.getId());
    }

    @Override
    protected Paiment sqlToEntity(ResultSet rs) throws SQLException {
        Integer id = rs.getInt("id");
        Date sqlDate = rs.getDate("date");
        LocalDate date = sqlDate.toLocalDate();
        String bankId = rs.getString("bankId");
        Float price = rs.getFloat("price");
        String status = rs.getString("status");
        return new Paiment(id, date, bankId, price, status);
    }
}
