package dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

import entities.ProjectHolder;

public class ProjectHolderDao extends AbstractDao<ProjectHolder> {

    public ProjectHolderDao(String addSQL, String getAllSQL, String getByIdSQL, String updateSQL, String deleteSQL) {
        super(
                "INSERT INTO projectHolder (firstname, lastname, mail, password, phonenumber, birthdate, image, address, city) VALUES (?,?,?,?,?,?,?,?,?)",
                "SELECT * FROM projectHolder",
                "SELECT * FROM projectHolder WHERE id=?",
                "UPDATE projectHolder SET firstname=?,lastname=?,mail=?, password=?, phonenumber=?, birthdate=?, image=?, address=?, city=? WHERE id=?",
                "DELETE FROM projectHolder WHERE id=?");
    }

    @Override
    protected ProjectHolder sqlToEntity(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        String firstname = rs.getString("firstname");
        String lastname = rs.getString("lastname");
        String mail = rs.getString("mail");
        String password = rs.getString("password");
        String phonenumber = rs.getString("phonenumber");
        Date sqlDate = rs.getDate("birthdate");
        LocalDate birthdate = sqlDate.toLocalDate();
        String image = rs.getString("image");
        String address = rs.getString("address");
        String city = rs.getString("city");
        return new ProjectHolder(id, firstname, lastname, mail, password, phonenumber, birthdate, image, address, city);
    }

    @Override
    protected void setEntityId(ProjectHolder entity, int id) {
        entity.setId(id);
    }

    @Override
    protected void entityBindValues(PreparedStatement stmt, ProjectHolder entity) throws SQLException {
        stmt.setString(1, entity.getFirstname());
        stmt.setString(2, entity.getLastname());
        stmt.setString(3, entity.getMail());
        stmt.setString(4, entity.getPassword());
        stmt.setString(5, entity.getPhonenumber());
        stmt.setDate(6, Date.valueOf(entity.getBirthdate()));
        stmt.setString(7, entity.getImage());
        stmt.setString(8, entity.getAddress());
        stmt.setString(9, entity.getCity());
    }

    @Override
    protected void entityBindValuesWithId(PreparedStatement stmt, ProjectHolder entity) throws SQLException {
        entityBindValues(stmt, entity);
        stmt.setInt(10, entity.getId());
    }

}
