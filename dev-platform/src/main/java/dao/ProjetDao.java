package dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import entities.Projet;

public class ProjetDao extends AbstractDao<Projet> {

    public ProjetDao(String addSQL, String getAllSQL, String getByIdSQL, String updateSQL, String deleteSQL) {
        super(
                "INSERT INTO Projet (title, description, price, startingDate, estimationDuration, status) VALUES (?,?,?,?,?,?)",
                "SELECT * FROM Projet",
                "SELECT * FROM Projet WHERE id=?",
                "UPDATE Projet SET title=?, description=?, price=?, startingDate=?, estimationDuration=?, status=? WHERE id=?",
                "DELETE FROM Projet WHERE id=?");
    }

    @Override
    protected void setEntityId(Projet entity, int id) {
        entity.setId(id);
    }

    @Override
    protected void entityBindValues(PreparedStatement stmt, Projet entity) throws SQLException {
        stmt.setString(1, entity.getTitle());
        stmt.setString(2, entity.getDescription());
        stmt.setInt(3, entity.getPrice());
        stmt.setDate(4, Date.valueOf(entity.getStartingDate()));
        stmt.setInt(5, entity.getEstimationDuration());
        stmt.setString(6, entity.getStatus());
    }

    @Override
    protected void entityBindValuesWithId(PreparedStatement stmt, Projet entity) throws SQLException {
        entityBindValues(stmt, entity);
        stmt.setInt(7, entity.getId());
    }

    @Override
    protected Projet sqlToEntity(ResultSet rs) throws SQLException {
        Integer id = rs.getInt("id");
        String title = rs.getString("title");
        String description = rs.getString("description");
        int price = rs.getInt("price");
        Date sqlDate = rs.getDate("startingDate");
        LocalDate startingDate = sqlDate.toLocalDate();
        int estimationDuration = rs.getInt("estimationDuration");
        String status = rs.getString("status");
        return new Projet(id, title, description, price, startingDate, estimationDuration, status);
    }
}
