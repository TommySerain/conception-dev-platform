package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import entities.Technology;

public class TechnologyDao extends AbstractDao<Technology> {

    public TechnologyDao(String addSQL, String getAllSQL, String getByIdSQL, String updateSQL, String deleteSQL) {
        super(
                "INSERT INTO Technology (name, devType) VALUES (?,?)",
                "SELECT * FROM Technology",
                "SELECT * FROM Technology WHERE id=?",
                "UPDATE Technology SET name=?, devType=? WHERE id=?",
                "DELETE FROM Technology WHERE id=?");
    }

    @Override
    protected void setEntityId(Technology entity, int id) {
        entity.setId(id);
    }

    @Override
    protected void entityBindValues(PreparedStatement stmt, Technology entity) throws SQLException {
        stmt.setString(1, entity.getName());
        stmt.setString(2, entity.getDevType());
    }

    @Override
    protected void entityBindValuesWithId(PreparedStatement stmt, Technology entity) throws SQLException {
        entityBindValues(stmt, entity);
        stmt.setInt(3, entity.getId());
    }

    @Override
    protected Technology sqlToEntity(ResultSet rs) throws SQLException {
        Integer id = rs.getInt("id");
        String name = rs.getString("name");
        String devType = rs.getString("devType");
        return new Technology(id, name, devType);
    }
}
