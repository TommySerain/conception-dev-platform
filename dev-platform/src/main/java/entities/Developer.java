package entities;

import java.time.LocalDate;

public class Developer extends User{
    private Integer id;
    private int dayRate;
    
    public Developer(Integer id, String firstname, String lastname, String mail, String password, String phonenumber,
            LocalDate birthdate, String image, String address, String city, int dayRate) {
        super(id, firstname, lastname, mail, password, phonenumber, birthdate, image, address, city);
        this.dayRate = dayRate;
    }
    public Developer(String firstname, String lastname, String mail, String password, String phonenumber,
            LocalDate birthdate, String image, String address, String city, int dayRate) {
        super(firstname, lastname, mail, password, phonenumber, birthdate, image, address, city);
        this.dayRate = dayRate;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public int getDayRate() {
        return dayRate;
    }
    public void setDayRate(int dayRate) {
        this.dayRate = dayRate;
    }

}
