package entities;

public class Devis {
    private Integer id;
    private String quoteNumber;
    private int totalPrice;
    private String type;
    private int duration;
    private String status;

    public Devis(Integer id, String quoteNumber, int totalPrice, String type, int duration, String status) {
        this.id = id;
        this.quoteNumber = quoteNumber;
        this.totalPrice = totalPrice;
        this.type = type;
        this.duration = duration;
        this.status = status;
    }
    public Double getAccount(){
        Double account = totalPrice * 0.3;
        return account;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getQuoteNumber() {
        return quoteNumber;
    }
    public void setQuoteNumber(String quoteNumber) {
        this.quoteNumber = quoteNumber;
    }
    public int getTotalPrice() {
        return totalPrice;
    }
    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public int getDuration() {
        return duration;
    }
    public void setDuration(int duration) {
        this.duration = duration;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
}
