package entities;

import java.time.LocalDate;

public class Evaluation {
    private Integer id;
    private int note;
    private String comments;
    private LocalDate date;

    
    public Evaluation(Integer id, int note, String comments, LocalDate date) {
        this.id = id;
        this.note = note;
        this.comments = comments;
        this.date = date;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public int getNote() {
        return note;
    }
    public void setNote(int note) {
        this.note = note;
    }
    public String getComments() {
        return comments;
    }
    public void setComments(String comments) {
        this.comments = comments;
    }
    public LocalDate getDate() {
        return date;
    }
    public void setDate(LocalDate date) {
        this.date = date;
    }
}
