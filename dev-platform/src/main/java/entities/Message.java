package entities;

import java.time.LocalDate;

public class Message {
    private Integer id;
    private String content;
    private LocalDate date;
    
    public Message(Integer id, String content, LocalDate date) {
        this.id = id;
        this.content = content;
        this.date = date;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public LocalDate getDate() {
        return date;
    }
    public void setDate(LocalDate date) {
        this.date = date;
    }
}
