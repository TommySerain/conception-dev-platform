package entities;

import java.time.LocalDate;

public class Paiment {
    private Integer id;
    private LocalDate date;
    private String bankId;
    private Float price;
    private String status;

    public Paiment(LocalDate date, String bankId, Float price, String status) {
        this.date = date;
        this.bankId = bankId;
        this.price = price;
        this.status = status;
    }

    public Paiment(Integer id, LocalDate date, String bankId, Float price, String status) {
        this.id = id;
        this.date = date;
        this.bankId = bankId;
        this.price = price;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
