package entities;

import java.time.LocalDate;

public class ProjectHolder extends User {
    public ProjectHolder(Integer id, String firstname, String lastname, String mail, String password,
            String phonenumber, LocalDate birthdate, String image, String address, String city) {
        super(id, firstname, lastname, mail, password, phonenumber, birthdate, image, address, city);
    }

    private Integer id;

    public ProjectHolder(String firstname, String lastname, String mail, String password, String phonenumber,
            LocalDate birthdate, String image, String address, String city) {
        super(firstname, lastname, mail, password, phonenumber, birthdate, image, address, city);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
