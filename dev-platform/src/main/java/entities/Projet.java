package entities;

import java.time.LocalDate;

public class Projet {
    private Integer id;
    private String title;
    private String description;
    private int price;
    private LocalDate startingDate;
    private int estimationDuration;
    private String status;

    public Projet(Integer id, String title, String description, int price, LocalDate startingDate,
            int estimationDuration, String status) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.price = price;
        this.startingDate = startingDate;
        this.estimationDuration = estimationDuration;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTirle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescrption(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public LocalDate getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(LocalDate startingDate) {
        this.startingDate = startingDate;
    }

    public int getEstimationDuration() {
        return estimationDuration;
    }

    public void setEstimationDuration(int estimationDuration) {
        this.estimationDuration = estimationDuration;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getDayRate() {
        int dayRate = price / estimationDuration;
        return dayRate;
    }
}
