package entities;

public class Technology {
    private Integer id;
    private String name;
    private String devType;
    
    public Technology(Integer id, String name, String devType) {
        this.id = id;
        this.name = name;
        this.devType = devType;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDevType() {
        return devType;
    }
    public void setDevType(String devType) {
        this.devType = devType;
    }
}
